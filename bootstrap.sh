#!/bin/sh
aclocal
autoconf
autoheader
# We want most files that automake adds by default, but not the GNU
# GPL license.
automake -a
rm COPYING
automake --foreign
